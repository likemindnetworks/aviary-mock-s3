# local S3
#
# VERSION       1.0.0

# based off node
FROM node:0.12.2
MAINTAINER Shen shen@likemindnetworks.com

RUN npm install -g s3rver
RUN mkdir -p /var/s3rver

EXPOSE 10001

# start s3rver
CMD ["s3rver", "-d", "/var/s3rver",  "-p", "10001", "-h", "0.0.0.0"]
